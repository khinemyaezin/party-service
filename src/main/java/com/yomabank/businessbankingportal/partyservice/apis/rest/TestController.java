package com.yomabank.businessbankingportal.partyservice.apis.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RestController
@RequestMapping("api/v1/test")
public class TestController {

    @GetMapping("/")
    public void test(){
        System.out.println("authorized");
    }
}
